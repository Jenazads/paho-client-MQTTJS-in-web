var app = angular.module('cifradoApp', ['app.Controllers']);

var appControllers = angular.module('app.Controllers',[])

appControllers.controller('mqttwsController', ['$scope',function($scope) {
  
  //var client = new MqttClient({host : 'localhost', port : 1884,});
  //var client = new Paho.MQTT.Client("localhost", Number(1884), "clientId");
  
  $scope.topic = "free/prueba/sin/data/"
  $scope.messageSubs = ""
  
  function setMessage(message){
    $scope.messageSubs = message
  }
  
  function newOption(){
    var options = {
      userName: 'Master',
      password: 'retsaM',
      useSSL: false,
      onSuccess: onConnect,
      onFailure: function (message) {
        console.log("Connection failed: " + message.errorMessage);
      }
    };
    return options
  }
  
  function NewClient(){
    var client = new Paho.MQTT.Client("localhost", 1884, "IdClient_WebClient");
    client.onConnectionLost = function (responseObject) {
      console.log("connection lost: " + responseObject.errorMessage);
    };
    return client
  }
  
  var client = NewClient()
  client.connect(newOption());

  var subscribe = function(cliente, topic, qosnum){
    cliente.subscribe(topic, {qos:qosnum})
  }
  
  var publish = function (cliente, payload, topic, qos) {
   var message = new Paho.MQTT.Message(payload);
   message.destinationName = topic;
   message.qos = qos;
   cliente.send(message);
  }
  
  function onConnect(){
    console.log("mqtt connected");
    subscribe(client, $scope.topic, 1);
    publish(client, "Meow",$scope.topic,1)
  }
  
  
  client.onMessageArrived = function (message) {
    console.log(message.destinationName, ' -- ', message.payloadString);
    console.log(message)
    console.log(typeof(message.payloadString))
    console.log(message.payloadString)
    setMessage(message.payloadString);
  };
  
  /*
  var client2 = mqtt.connect("wss://localhost:2884", tls.connect({
    ca: [ caFile ],
    cert: certFile,
    key: keyFile,
    rejectUnauthorized: false,
    port: Meteor.settings.mqtt.port,
    host: Meteor.settings.mqtt.host,
    secureProtocol: 'TLSv1_method'
  }));
  */
}]);
